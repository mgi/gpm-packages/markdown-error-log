﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@mgi" Type="Folder">
				<Item Name="detailed-error" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Detailed Error.lvlib" Type="Library" URL="../gpm_packages/@mgi/detailed-error/Source/MGI-Detailed Error.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/detailed-error/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/detailed-error/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/detailed-error/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/detailed-error/README.md"/>
				</Item>
				<Item Name="error-log" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Error Log.lvlib" Type="Library" URL="../gpm_packages/@mgi/error-log/Source/MGI-Error Log.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/error-log/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/error-log/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/error-log/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/error-log/README.md"/>
				</Item>
				<Item Name="library-core" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI Non Repeating Plot Color.vi" Type="VI" URL="../gpm_packages/@mgi/library-core/Source/MGI Non Repeating Plot Color.vi"/>
						<Item Name="MGI-Library Core.lvlib" Type="Library" URL="../gpm_packages/@mgi/library-core/Source/MGI-Library Core.lvlib"/>
						<Item Name="Suppress Error Code (Array).vi" Type="VI" URL="../gpm_packages/@mgi/library-core/Source/Suppress Error Code (Array).vi"/>
						<Item Name="Suppress Error Code.vim" Type="VI" URL="../gpm_packages/@mgi/library-core/Source/Suppress Error Code.vim"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/library-core/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/library-core/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/library-core/LICENSE"/>
					<Item Name="readme.md" Type="Document" URL="../gpm_packages/@mgi/library-core/readme.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="MGI-Markdown Error Log.lvlib" Type="Library" URL="../Source/MGI-Markdown Error Log.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Number To Enum.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Number To Enum.vim"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
